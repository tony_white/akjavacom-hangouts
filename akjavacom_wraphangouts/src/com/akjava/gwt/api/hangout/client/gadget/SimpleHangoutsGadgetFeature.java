package com.akjava.gwt.api.hangout.client.gadget;

import com.google.gwt.gadgets.client.GadgetFeature.FeatureName;

@FeatureName({"rpc","dynamic-height","views"})
public interface SimpleHangoutsGadgetFeature {

}
